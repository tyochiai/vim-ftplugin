function! s:_LessCompile()
    silent exe ":!lessc %:p > %:p:r.css"
endfunction

augroup _LESS
    autocmd!
    autocmd BufWritePost * call s:_LessCompile()
augroup END
